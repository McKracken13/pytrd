.. _topics-index:

===========================
UCT-ALICE-TRD documentation
===========================

The physics department at the University of Cape Town is in possession of a Transition Radiation Detector (TRD), similar to those used on the ALICE experiment at CERN. This is a python project designed to make reading, processing, and visualising the data acquired from the TRD as easy and as extensible as possible.

This project was adapted from the initial pytrd code which can be found `here`_. A new repository was created as the whole code structure changed.

.. _here: https://bitbucket.org/uctalice/pytrd/src/master/

Installation
============

PyTRD can be installed by downloading the `pytrd`_ file from the repository and placing it into the 'Lib' folder in the main Python directory. 

.. _pytrd: https://gitlab.com/McKracken13/pytrd/raw/master/pytrd.py?inline=false

PyTRD Classes
=============

.. toctree::
	:caption: PyTRD Classes
	:hidden:

	
	classes/trdfile
	classes/trdevent
	classes/visual
	classes/linear
	classes/DataFormatException

:doc:`classes/trdfile`
	Class that opens and processes the raw data received from the TRD.
:doc:`classes/trdevent`
	Class that manages the data for 1 single detection event.
:doc:`classes/visual`
	Class that contains tools for visualising TRD events.
:doc:`classes/linear`
	Class containing linear fitting tools.
:doc:`classes/DataFormatException`
	Raised when the data format is invalid.

DAQ System
==========

.. toctree::
	:caption: DAQ System
	:hidden:
.. TBD:
