trdfile
=======

Creates a new instance of a TRD file using the file name provided. The TRD file is a basic list implementation of the raw data contained in the file provided. Each item in the list is a dictionary that contains the version of the data format used, the time stamp of the event, the number of data segments and the list containing the data segments. Each data segment is a dictionary containing the sfp number, the size of the segment, and the list of the raw data in the segment.

Custom methods:

     from_json(filename)
          Returns an event dictionary from a json object file

     save_json(self, filename)
          Saves the raw data to a json object file

Magic methods:

     __getitem__(self, key)
          Return self[key]

     __len__(self)
          Return len(self)

     __str__(self)
          Return str(self)

