DataFormatException
===================

This is an exception that is raided when the format of the dat received by the program is invalid. It can be caused by zero suppressing the data from the TRD. It can be an indicator that there is something wrong with the electronics on the TRD.
