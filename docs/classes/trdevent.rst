trdevent
========

Class that manages the data for 1 single detection event. A TRD event has several attributes.


version: The current version of the format of the data blocks.

time: The time the run was started.

segments: The number of data segments in the event.

blocks: The list containing the raw data blocks.

data_cube: The numpy array containing the processed data.


TRD events support numpy-like indexing where trdevent[slice] is equivalent to data_cube[slice].

Custom methods:

     ave_signal(self)
          Returns the average number of counts throught the whole data block.

     get_axes(self)
          Returns the real space X, Y, and Z axes.
     
     regions_of_interest(self)
          Returns an iterator of the all the regions of interest in the data cube.
     
     set_drift(self, zdrift, uzdrift=0)
          Method for setting the drift velocity of the electrons. Defaults to 1,uncertainty default to 0.
     
     set_mask(self, mask, value=0)
          Method for bulk setting values in the data cube. Useful for areas of the TRD that are returning garbage data.

Magic methods:

     __getitem__(self, key)
         Return self[key]

     __len__(self)
         Return len(self)

     __str__(self)
         Return str(self)

