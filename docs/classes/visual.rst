visual
======

This is a class containing tools helpful for visualising the data stored in the data cube.

Methods defined here:

     get_tracks(self)
          Finds and returns the cosmic rays based on a linear fit of interesting regions.

     plot_tracks(self, tracks)
          Method that plots position of the intersection of the track with the xy-plane, given the depth of the plane z and the axes.

     pulse_height(self, show=True)
          Displays a pulse height graph for the event.

     slices(self)
          Displays an interactive graph showing the ADC data for each slice of the data cube.

     stack(self)
          Displays a plot of the summed count values along the time bin axis.

     stack_tracks(self, tracks)
          Displays a plot of the summed count values along the time bin axis as well as plotting lines showing the particle tracks through the detector.