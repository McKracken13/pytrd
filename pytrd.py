#Necessary Imports
import numpy as np
import re
import bz2
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
import matplotlib.patches as patches
from datetime import datetime
from scipy.optimize import curve_fit

VERSION = '1.0'
ANGLE_LIM = 18/np.pi

class DataFormatException(Exception):
	"""Raised when the data format is invalid."""
	pass

class linear:
	"""Class containing linear fitting tools."""

	def linear_1d(x,y,uy):
		"""Finds the values for m and c that minimises the sum of the squares of the
		residuals weighted by the uncertainty squared."""
		def l(x,m,c):
			return m*x+c
		popt, pcov = curve_fit(l,x,y,sigma=uy,p0=[1,1],absolute_sigma=True)
		m,c = popt
		dymin = (y- l(x,*popt))/uy 
		min_chisq = np.sum(dymin**2) 
		dof = len(x) - len(popt) 
		um, uc = (np.sqrt(pcov[i,i])*np.sqrt(min_chisq/dof) for  i in range(len(popt)))
		return (m ,c ,um ,uc,min_chisq/dof)

	def linear_fit(points):
		"""Returns a linear fit of 3 dimensional data."""
		x = np.array([point[0][0] for point in points])
		y = np.array([point[0][1] for point in points])
		z = np.array([point[0][2] for point in points])
		ux = np.array([point[1][0] for point in points])
		uy = np.array([point[1][1] for point in points])
		uz = np.array([point[1][2] for point in points])
		linx = linear.linear_1d(z,x,ux)
		liny = linear.linear_1d(z,y,uy)
		M = np.array([linx[0],liny[0],1])
		uM = np.array([linx[2],liny[2],0])
		C = np.array([linx[1],liny[1],0])
		uC = np.array([linx[3],liny[3],0])
		rchi2 = (linx[-1]+liny[-1])/2
		return (M,C,uM,uC,rchi2)

	def linear_function(z,M,C):
		return M*np.stack((z,z,z),axis=-1)+C

class trdevent:
	"""
	trdevent(dict) -> trdevent

	Creates a new instance of a TRD event. A TRD event has several attributes.

	version - The current version of the format of the data blocks.
	time - The time the run was started.
	segments - The number of data segments in the event.
	blocks - The list containing the raw data blocks.
	data_cube - The numpy array containing the processed data.

	TRD events support numpy-like indexing where trdevent[slice] is equivalent
	to data_cube[slice].

	"""
	__EoT = int('0x10001000',0)

	def __init__(self,**event_dict):
		self.version = event_dict['version']
		self.time = event_dict['time']
		self.segments = event_dict['segments']
		self.blocks = event_dict['blocks']
		self.data_cube = np.zeros((16,144,30))
		self.xscale = 0.0891
		self.yscale = 0.0077
		self.zdrift = 0.01/6.67
		self.ux = self.xscale/np.sqrt(6)
		self.uy = self.yscale/np.sqrt(6)
		self.uz = self.zdrift/np.sqrt(6)

		if self.version != VERSION:
			raise DataFormatException('Invalid file format. PyTRD version %s does not support version %s!'%(VERSION,self.version))

		for sfp in range(self.segments):
			try :
				tracklet_end = 0
				entry = 0
				value_iter = iter(self.blocks[sfp]['data'])
				while tracklet_end < 2:
					raw_value = next(value_iter)

					if raw_value == self.__EoT:
						tracklet_end += 1

				header = next(value_iter)

				major = (header >> 24) & 0x7f 
				minor = (header >> 17) & 0x7f 
				nhw   = (header >> 14) & 0x7
				sm    = (header >>  9) & 0x7
				layer = (header >>  6) & 0x1f
				stack = (header >>  3) & 0x3
				side  = (header >>  2) & 0x1
				sidestr = 'A' if side==0 else 'B'
				header_words = (int(hex(header)[0:10],0) >> 14) & 0x7

				if header_words >= 1:
					HC1_header = next(value_iter)
					ntb         = (HC1_header >> 26) & 0x3f
					bc_counter  = (HC1_header >> 10) & 0xffff
					pre_counter = (HC1_header >>  6) & 0xF
					pre_phase   = (HC1_header >>  2) & 0xF
					ntb = (int(hex(HC1_header)[0:10],0) >> 26) & 0x3F
					for i in range(1,header_words):
						check = next(value(iter))

				if header_words >= 2:
					hc2 = next(value(iter))

				if header_words >= 3:
					hc3 = next(value(iter))

				for read_out_board in range(3):
					for multichip_module in range(16):
						MCM_header = next(value_iter)

						if major & (1<<5):
							adcmask = next(value_iter)
						else:
							adcmask = 0x01FFFFFC

						for channel in range(21):
						
							if (adcmask & (1 << (channel + 4))) == 0:
								continue

							for i in range((ntb-1)//3 + 1):

								value = next(value_iter)
								timebin = i*3

								if channel >= 0 and channel < 18:
									ROB = (int(hex(MCM_header),0) >> 28) & 0x7
									MCMpos = (int(hex(MCM_header),0) >> 24) & 0xF
									col = 7 - (4 * (ROB % 2) + MCMpos % 4)
									row = 4 * ((ROB // 2)) + (MCMpos // 4)
									self.data_cube[row][col * 18 + channel][timebin + 2] 	= (value >> 22) & 0x3ff
									self.data_cube[row][col * 18 + channel][timebin + 1] 	= (value >> 12) & 0x3ff
									self.data_cube[row][col * 18 + channel][timebin] 		= (value >> 2) & 0x3ff
				
			except StopIteration as e:
				raise DataFormatException("Reached end of data while iterating. Format is invalid.")
		self.data_cube = self.data_cube[:12]

	def __getitem__(self,key):
		"""Return self[key]"""
		return self.data_cube[key]

	def __str__(self):
		"""Return str(self)"""
		return str(self.data_cube)

	def __len__(self):
		"""Return len(self)"""
		return len(self.data_cube)

	def set_mask(self,mask,value = 0):
		self.data_cube[mask] = value

	def set_drift(self,zdrift,uzdrift=0):
		"""Method for setting the drift velocity of the electrons. Defaults to 1, 
		uncertainty default to 0."""
		self.zdrift = zdrift
		self.uzdrift = uzdrift

	def get_axes(self):
		"""Returns the real space X,Y, and Z axes."""
		X,Y,Z = self.xscale*np.arange(0,12,1),self.yscale*np.arange(0,144,1),self.zdrift*np.arange(0,30,1)
		uX,uY,uZ = self.ux*np.ones(12),self.uy*np.ones(144),self.uz*np.ones(30)
		return X,Y,Z,uX,uY,uZ

	def regions_of_interest(self):
		"""Returns an iterator of the all the regions of interest in the data cube."""
		tbsum = np.sum(self.data_cube, 2)
		poi = np.argwhere(tbsum > 350)
		roi = []
		for r in sorted(set(poi[:,0])):
			pads = [x[1] for x in poi if x[0]==r]
			start = False
			current = False
			for p in pads+[999]:
				if not start:
					start=p
					current=p-1
				if p==(current+1):
					current = p
				else:
					npad = current-start+1
					roi.append( {'row': r,'start': start,'end': start+npad-1,'npad': npad} )
		return iter(roi)

	def ave_signal(self):
		return np.mean(self.data_cube)
		


class trdfile:
	"""
	trdfile(string) -> trdfile

	Creates a new instance of a TRD file using the file name provided. The TRD file 
	is a basic list implementation of the raw data contained in the file provided.
	Each item in the list is a dictionary that contains the version of the data 
	format used, the time stamp of the event, the number of data segments and the 
	list containing the data segments. Each data segment is a dictionary containing
	the sfp number, the size of the segment, and the list of the raw data in the 
	segment.
	"""

	def __next_line(self):
		"""Next line protocol for calling the next line in the file"""
		return self.file.readline().strip()

	def __init__(self, filename,compressed=False):
		if compressed:
			self.file = bz2.open(filename,mode='rt')
		else:
			self.file = open(filename,mode='r')


		line = self.__next_line()

		if line != '# EVENT':
			raise DataFormatException('Invalid file format. Expected "# EVENT", got "%s"!'%(line,))

		# self.num_events = 1
		self.events = []

		while line == '# EVENT':
			try:

				format_field = re.search('# *format version: *(.*)', self.__next_line())
				self.file_format = format_field.group(1)

				if self.file_format == VERSION:
					header_fields = ('time stamp', 'data blocks')
				else:
					raise DataFormatException('Invalid file format. PyTRD version %s does not support version %s!'%(VERSION,self.file_format))

				for field in header_fields:

					if field == 'time stamp':
						time_field = re.search('# *time stamp: *(.*)', self.__next_line())
						self.timestamp = datetime.strptime( time_field.group(1),'%Y-%m-%dT%H:%M:%S.%f')

					elif field == 'data blocks':
						data_blocks_field = re.search('# *data blocks: *(.*)', self.__next_line())
						self.num_blocks = int(data_blocks_field.group(1))

					else:
						raise DataFormatException('Invalid file format. Header field "%s" is not recognised!'%(field,))
				
				self.blocks = []

				for block in range(self.num_blocks):
					current_block = dict()
					line = self.__next_line()

					if line != '## DATA SEGMENT':
						raise DataFormatException('Invalid file format. Expected "## DATA SEGMENT", got "%s"!'%(line,))
					
					block_fields = ('sfp', 'size')

					for field in block_fields:

						if field == 'sfp':
							sfp_field = re.match('## *sfp: *(.*)', self.__next_line())
							current_block['sfp'] = int(sfp_field.group(1))

						elif field == 'size':
							size_field = re.match('## *size: *(.*)', self.__next_line())
							current_block['size'] = int(size_field.group(1))

						else:
							raise DataFormatException('Invalid file format. Data segment field "%s" is not recognised!'&(field,))

					current_block['data'] = np.zeros(current_block['size'],dtype=np.uint32)

					for i in range(current_block['size']):
						current_block['data'][i] = int(self.__next_line(),0)

					self.blocks.append(current_block)


				self.events.append(
					dict(version = self.file_format,
						time = self.timestamp,
						segments = self.num_blocks,
						blocks = self.blocks)
					)

				line = self.__next_line()

			except :
				raise DataFormatException('Encountered error while reading data.')
				break

		self.file.close()

	def __str__(self):
		"""Return str(self)"""
		return str(self.events)

	def __len__(self):
		"""Return len(self)"""
		return len(self.events)

	def __getitem__(self,key):
		"""Return self[key]"""
		return self.events[key]

	def save_json(self,filename):
		"""Saves the raw data to a json object """
		json_file = open(filename+'.json','wb')
		print(json.dumps(self.events),file=json_file)
		json_file.close()

	def from_json(filename):
		"""Saves the raw data to a json object """
		json_file = open(filename+'.json','rb')
		data = json.loads("".join(json_file.readlines()))
		json_file.close()
		return data
import matplotlib.colors as colors

class visual:
	"""
	visual(trdevent) -> visual

	Class for visualising the data stored in the data cube.
	"""
	def __init__(self, event):
		self.event = event
		self.vmin = np.min(self.event.data_cube)
		self.vmax = np.max(self.event.data_cube)

	def stack(self):
		"""
		Displays a plot of the summed count values along the time bin axis.
		"""
		ax = plt.subplot(111)
		x,y,z,ux,uy,uz = self.event.get_axes()
		plt.contourf(x,y,np.transpose(np.sum(self.event.data_cube,axis=2)),5,cmap='hot')
		c = 0
		for roi in self.event.regions_of_interest():
			c += 1
			p = (self.event.xscale*(roi['row']-1),self.event.yscale*roi['start'])
			w = 2*self.event.xscale
			h = self.event.yscale*roi['npad']
			rect = patches.Rectangle(p,w,h,linewidth=1,edgecolor='r',facecolor='none')
			ax.add_patch(rect)
		plt.xlabel('x')
		plt.ylabel('y')
		plt.colorbar()
		plt.show()

	def slices(self):
		"""
		Displays an interactive graph showing the ADC data for each slice of the
		data cube.
		"""
		fig = plt.figure()
		ax = plt.axes([0.1,0.2,0.8,0.75])
		ax.set(xlabel='x',ylabel='y')
		x,y,z,ux,uy,uz = self.event.get_axes()
		ax.margins(x=0)
		axlayer = plt.axes([0.1, 0.1, 0.8, 0.03],)
		slayer = Slider(axlayer, 'Layer', z[0], z[-1], valinit=0, valstep=self.event.zdrift)
		layer = slayer.val
		cf = ax.contourf(x,y,np.transpose(self.event.data_cube[:,:,int(round(layer/self.event.zdrift))]),5,vmin=self.vmin,vmax=self.vmax)
		def draw(val):
			layer = slayer.val
			ax.cla()
			xy = self.event.data_cube[:,:,int(round(layer/self.event.zdrift))]
			cf = ax.contourf(x,y,np.transpose(xy)+0.1,5,norm=colors.LogNorm(vmin=self.vmin+0.1, vmax=self.vmax+0.1))
			ax.set_title('Max: %d, Min: %d'%(np.max(xy),np.min(xy)))
			ax.set(xlabel='x',ylabel='y')
			plt.draw()
		slayer.on_changed(draw)

		plt.show()

	def pulse_height(self,show=True):
		"""
		Displays a pulse height graph for the event.
		"""
		DATA_EXCLUDE_MASK = np.zeros((12, 144, 30), dtype=bool)
		DATA_EXCLUDE_MASK[4:8,0:72,:] = True
		tbsum_mask = 320
		sumtrkl = np.zeros(30)
		ntrkl = 0
		data = self.event.data_cube
		data[DATA_EXCLUDE_MASK] = 0
		for roi in self.event.regions_of_interest():
			trkl = data[roi['row'], roi['start']:roi['end'], :]-9.5
			if ( np.sum(trkl[:,0:6]) > 50 ): continue
			sumtrkl += np.sum(trkl, 0)
			ntrkl += 1
			plt.plot(np.sum(trkl,0))
		if show :
			plt.figure()
			plt.plot(sumtrkl/ntrkl)
			plt.show()
		return sumtrkl/ntrkl

	def get_tracks(self):
		"""
		Finds and returns the cosmic rays based on a linear fit of interesting 
		regions.
		"""
		def get_regions(array):
			regions = []
			start = False
			ind = 0
			for i,truth in enumerate(array):
				if truth and not start:
					start = True
					ind = i
				if not truth and start:
					start = False
					regions.append((ind,i))


			if start:
				regions.append((ind,i+1))
			return regions

		def merge_events(e1,e2):
			pad1, pad2 = e1[0][0], e2[0][0]
			ratio = e1[2]/e2[2]
			if abs(pad1 - pad2) == 1 and np.max(ratio,1/ratio) > 2:
				coord = (np.average([e1[0][0], e2[0][0]],weights = [e1[2],e2[2]]),np.average([e1[0][1], e2[0][1]],weights = [e1[2],e2[2]]),z[timebin])
				u_coord = (np.average([e1[1][0]**2, e2[1][0]**2],weights = [e1[2],e2[2]])**0.5,np.average([e1[1][1]**2, e2[1][1]**2],weights = [e1[2],e2[2]])**0.5,uz)
				return ((coord,u_coord,counts),)
			else:
				return (e1,e2)

		def in_cone(ev1,ev2):
			return np.sqrt((ev1[0][0]-ev2[0][0])**2+(ev1[0][1]-ev2[0][1])**2) < 8*abs(ev1[0][2]-ev2[0][2])

		row_index = np.arange(0,144,1)

		x,y,z,ux,uy,uz = self.event.get_axes()
		detection_events = []
		for timebin in range(self.event.data_cube.shape[-1]):
			layer = self.event[:12,:,timebin]
			pad_events = []
			event_set = set()
			for pad in range(12):
				row = layer[pad]
				interesting = row > 100
				regions = get_regions(interesting)
				peaks = [int(0.5*(a[0]+a[1]-1)) for a in regions]
				for peak in peaks:
					if peak == 0 or peak==143: 
						continue
					outer = min(5,peak)
					peak_mask = (row_index >= (peak - outer )) & (row_index < (peak + outer ))
					# print(peak_mask.shape)
					while (len(get_regions(peak_mask & interesting)) != 1) & (outer >0):
						if outer == 0:
							raise Exception('Something has gone horribly wrong!')
						else:
							outer -=1
							peak_mask = (row_index > (peak - outer )) & (row_index < (peak + outer ))
					w = peak_mask.astype(int)*row
					centre_y = np.average(y,weights = w)
					variance = np.average((y-centre_y)**2 ,weights = w)
					u_centre_y = variance**0.5
					coord, u_coord = (x[pad],centre_y,z[timebin]), (ux[0],u_centre_y,uz[0])
					counts = np.sum(peak_mask.astype(int)*row)
					pad_events.append((coord,u_coord,counts))
			for i in range(len(pad_events)-1):
				for j in range(i,len(pad_events)):
					e1 = pad_events[i]
					e2 = pad_events[j]
					event_set.update({*merge_events(e1,e2)})
					break
			detection_events.append(list(event_set))
		tracks = []
		for pad_events in detection_events:
			for ev in pad_events:
				new_track = True
				for i,track in enumerate(tracks):
					if all([in_cone(p,ev) for p in track]):
						new_track = False
						track.append(ev)
						break
				if new_track:
					tracks.append([ev])
		fit_tracks = []
		for track in tracks:
			if len(track) < 11:
				continue
			else:
				fit_params = linear.linear_fit(track)
				try:
					if np.any(fit_params[2]/fit_params[0] > 1): continue
				except:
					pass
				if fit_params[-1] > 5: continue
				fit_tracks.append(fit_params)
		return fit_tracks

	def plot_tracks(self,tracks):
		"""
		Method that plots position of the intersection of the track with the 
		xy-plane, given the depth of the plane z and the axes.
		"""

		x,y,z,ux,uy,uz = self.event.get_axes()
		fig = plt.figure()
		ax = plt.axes([0.1,0.2,0.8,0.75])
		ax.set(xlabel='x',ylabel='y')
		ax.margins(x=0)
		axlayer = plt.axes([0.1, 0.1, 0.8, 0.03],)
		slayer = Slider(axlayer, 'Layer', z[0], z[-1], valinit=0, valstep=self.event.zdrift)
		def draw(val):
			layer = slayer.val
			zpos = int(layer/self.event.zdrift)
			ax.cla()
			xy = self.event.data_cube[:,:,zpos]
			cf = ax.contourf(x,y,np.transpose(self.event.data_cube[:,:,zpos]),10,vmin=self.vmin,vmax=self.vmax,cmap='hot')
			ax.set(xlabel='x',ylabel='y')
			for track in tracks:
				xp,yp,zp = np.transpose(track[0]*np.stack((z,z,z),axis=-1)+track[1])
				uxp,uyp,uzp = np.transpose(track[2]*np.stack((z,z,z),axis=-1)+track[3])
				# ax.errorbar([xp[zpos]],[yp[zpos]],xerr=[uxp[zpos]],yerr=[uyp[zpos]],fmt='bx',ecolor='b',capsize=2)
				ax.plot([xp[zpos]],[yp[zpos]],'bo')
			plt.draw()
		draw(0)
		slayer.on_changed(draw)
		plt.show()

	def stack_tracks(self,tracks):
		"""
		Displays a plot of the summed count values along the time bin axis as well
		as plotting lines showing the particle tracks through the detector.
		"""
		ax = plt.subplot(111)
		x,y,z,ux,uy,uz = self.event.get_axes()
		plt.contourf(x,y,np.transpose(np.sum(self.event.data_cube,axis=2)),5,cmap='hot')
		c = 0
		# for roi in self.event.regions_of_interest():
		# 	c += 1
		# 	p = (self.event.xscale*(roi['row']-1),self.event.yscale*roi['start'])
		# 	w = 2*self.event.xscale
		# 	h = self.event.yscale*roi['npad']
		# 	rect = patches.Rectangle(p,w,h,linewidth=1,edgecolor='r',facecolor='none')
		# 	ax.add_patch(rect)
		for track in tracks:
			xp,yp,zp = np.transpose(track[0]*np.stack((z,z,z),axis=-1)+track[1])
			uxp,uyp,uzp = np.transpose(track[2]*np.stack((z,z,z),axis=-1)+track[3])
			ax.errorbar(xp,yp,xerr=uxp,yerr=uyp,fmt='bx',ecolor='b',capsize=2)
		plt.xlabel('$x$ [m]')
		plt.ylabel('$y$ [m]')
		plt.colorbar()
		plt.show()
